package org.miage.parkbuddymiage;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.AsyncTask;

import org.miage.parkbuddymiage.controller.LocationProviderService;
import org.miage.parkbuddymiage.controller.NotificationControl;
import org.miage.parkbuddymiage.controller.ParkingManager;
import org.miage.parkbuddymiage.model.ParkingDataBase;
import org.miage.parkbuddymiage.model.Settings;

public class App extends Application {
    @SuppressLint("StaticFieldLeak")
    @Override
    public void onCreate() {
        super.onCreate();
        ParkingDataBase.Init(getApplicationContext());
        ParkingManager.updateParkingsFromDistantApi();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                ParkingDataBase.get().getSettings();
                if(Settings.INSTANCE == null)
                    new Settings();
                return null;
            }
        }.execute();
        NotificationControl.createNotificationChannel(getApplicationContext());
        LocationProviderService.init(getApplicationContext());
        LocationProviderService.INSTANCE().startProviding();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        LocationProviderService.INSTANCE().endProviding();
    }
}
