package org.miage.parkbuddymiage.model.opendatanantes;

import com.google.gson.annotations.Expose;

import org.miage.parkbuddymiage.model.Parking;

import java.util.List;

import lombok.Getter;
import lombok.ToString;

@ToString
public class AvailabilitySearchResult {

    /**
     * Nombre total de lignes
     */
    @Expose
    @Getter
    int nhits;

    public int getNextStart() {
        return parameters.start + parameters.rows;
    }

    /**
     * Paramètres de la réponse
     */
    @Expose @Getter
    Parameters parameters;
    @ToString
    public class Parameters {
        /**
         * Nombre de lignes affichées
         */
        @Expose @Getter
        int rows;
        /**
         * Ligne de départ
         */
        @Expose @Getter
        int start;
    }

    /**
     * Liste des enregistrements renvoyés
     */
    @Expose @Getter
    List<AvailabilityResult> records;
    @ToString
    public class AvailabilityResult {
        /**
         * Identifiant de l'enregistrement
         */
        @Expose @Getter
        String recordid;

        /**
         * Champs de l'enregistrement
         */
        @Expose @Getter
        Fields fields;
        @ToString
        public class Fields {
            /**
             * Identifiant du parking
             */
            @Expose @Getter
            int idobj;
            /**
             * Nombre de places disponibles
             */
            @Expose @Getter
            int grp_disponible;
            /**
             * Nombre de places minimum pour afficher complet
             */
            @Expose @Getter
            int grp_complet;
            /**
             * Statut
             */
            @Expose @Getter
            int grp_statut;
            public Parking.AvailabilityStatus getStatus() {
                switch (grp_statut) {
                    case 0:
                        return Parking.AvailabilityStatus.OUT_OF_ORDER;
                    case 1:
                        return Parking.AvailabilityStatus.CLOSED;
                    case 2:
                        return Parking.AvailabilityStatus.SUBSCRIBERS;
                    case 5:
                        return Parking.AvailabilityStatus.OPEN;
                }
                return Parking.AvailabilityStatus.OUT_OF_ORDER;
            }
        }
    }
}
