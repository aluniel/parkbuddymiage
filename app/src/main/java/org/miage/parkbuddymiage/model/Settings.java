package org.miage.parkbuddymiage.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.AsyncTask;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(prefix = "m")
@Entity(tableName = "Settings")
public class Settings {

    public static Settings INSTANCE;
    @PrimaryKey
    @Getter @Setter
    private int mId = 0;
    @Getter @Setter
    private boolean mNotificationActivated;

    public Settings() {
        INSTANCE = this;
    }

    @Ignore
    public void changeNotificationActivated(boolean notificationActivated) {
        mNotificationActivated = notificationActivated;
        save();
    }

    private void save() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                ParkingDataBase.get().updateSettings();
                return null;
            }
        }.execute();
    }
}
