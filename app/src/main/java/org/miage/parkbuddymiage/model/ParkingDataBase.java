package org.miage.parkbuddymiage.model;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

@Database(entities     = {Parking.class, Schedule.class, Settings.class},
          version      = 10,
          exportSchema = false)
@TypeConverters(DatabaseConverter.class)
public abstract class ParkingDataBase extends RoomDatabase {
    private static ParkingDataBase DATABASE;
    public static void Init(Context context) {
        DATABASE = Room
                .databaseBuilder(context, ParkingDataBase.class, "PARKING_DB")
                .fallbackToDestructiveMigration()
                .build();
    }
    public static ParkingDao get() {
        return DATABASE.parkingDao();
    }

    public abstract ParkingDao parkingDao();
}
