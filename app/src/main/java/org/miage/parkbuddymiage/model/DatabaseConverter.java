package org.miage.parkbuddymiage.model;

import android.arch.persistence.room.TypeConverter;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableDouble;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;

class DatabaseConverter {

    @TypeConverter
    public static ObservableField<String> toObversableFieldString(String value) {
        return new ObservableField<>(value);
    }

    @TypeConverter
    public static String toString(ObservableField<String> value) {
        return value.get();
    }

    @TypeConverter
    public static ObservableField<Parking.AvailabilityStatus> toObversableFieldAvailabilityStatus(Parking.AvailabilityStatus value) {
        return new ObservableField<>(value);
    }

    @TypeConverter
    public static Parking.AvailabilityStatus toAvailabilityStatus(ObservableField<Parking.AvailabilityStatus> value) {
        return value.get();
    }

    @TypeConverter
    public static ObservableDouble toObversableDouble(double value) {
        return new ObservableDouble(value);
    }

    @TypeConverter
    public static Double toDouble(ObservableDouble value) {
        return value.get();
    }

    @TypeConverter
    public static ObservableInt toObversableInt(int value) {
        return new ObservableInt(value);
    }

    @TypeConverter
    public static int toInteger(ObservableInt value) {
        return value.get();
    }

    @TypeConverter
    public static ObservableBoolean toObversableBoolean(boolean value) {
        return new ObservableBoolean(value);
    }

    @TypeConverter
    public static boolean toBoolean(ObservableBoolean value) {
        return value.get();
    }

    @TypeConverter
    public static Parking.AvailabilityStatus toStatus(int status) {
        for(Parking.AvailabilityStatus as : Parking.AvailabilityStatus.values())
            if(as.ordinal() == status)
                return as;
        return null;
    }
    @TypeConverter
    public static int toInteger(Parking.AvailabilityStatus status) {
        if(status == null)
            return -1;
        return status.ordinal();
    }

    @TypeConverter
    public static DayOfWeek toDayOfWeek(int dayOfWeek) {
        for(DayOfWeek day : DayOfWeek.values())
            if(day.ordinal() == dayOfWeek)
                return day;
        return null;
    }
    @TypeConverter
    public static int toInteger(DayOfWeek day) {
        if(day == null)
            return -1;
        return day.ordinal();
    }
    @TypeConverter
    public static LocalTime toLocalTime(int localTime) {
        if(localTime == -1)
            return null;
        return LocalTime.ofSecondOfDay(localTime);
    }
    @TypeConverter
    public static int toInteger(LocalTime time) {
        if(time == null)
            return -1;
        return time.toSecondOfDay();
    }
    @TypeConverter
    public static LocalDate toLocalDate(long localDate) {
        if(localDate == -1)
            return null;
        return LocalDate.ofEpochDay(localDate);
    }
    @TypeConverter
    public static long toLong(LocalDate date) {
        if(date == null)
            return -1;
        return date.toEpochDay();
    }
}