package org.miage.parkbuddymiage.model.opendatanantes;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ParkingSearchRESTService {
    /**
     * URL de base de l'API OpenDataNantes
     */
    String BASE_URL = "https://data.nantesmetropole.fr/";
    /**
     * Adresse interne de l'API RECORDS
     */
    String API_URL = "api/records/1.0/search";
    /**
     * Identifiant du dataset listant les parkings
     */
    String PARKING_DATASET = "244400404_parkings-publics-nantes";
    /**
     * Identifiant du dataset listant la disponibilité des parkings
     */
    String AVAILABILITY_DATASET = "244400404_parkings-publics-nantes-disponibilites";
    /**
     * Identifiant du dataset listant les horaires des parkings
     */
    String SCHEDULE_DATASET = "244400404_parkings-publics-nantes-horaires";
    /**
     * Nombre d'enregistrement par requête
     */
    int ROWS_PER_REQUEST = 100;

    @GET(API_URL + "?dataset=" + PARKING_DATASET + "&rows=" + ROWS_PER_REQUEST)
    Call<ParkingSearchResult> searchForParkings(@Query("start") int start);

    @GET(API_URL + "?dataset=" + SCHEDULE_DATASET + "&rows=" + ROWS_PER_REQUEST)
    Call<ScheduleSearchResult> searchForSchedule(@Query("start") int start);

    @GET(API_URL + "?dataset=" + AVAILABILITY_DATASET + "&rows=" + ROWS_PER_REQUEST)
    Call<AvailabilitySearchResult> searchForAvailability(@Query("start") int start);
}
