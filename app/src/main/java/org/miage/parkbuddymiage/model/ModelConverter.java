package org.miage.parkbuddymiage.model;

import android.util.Log;

import org.miage.parkbuddymiage.model.opendatanantes.AvailabilitySearchResult;
import org.miage.parkbuddymiage.model.opendatanantes.ParkingSearchResult;
import org.miage.parkbuddymiage.model.opendatanantes.ScheduleSearchResult;

import java.util.ArrayList;
import java.util.List;

public class ModelConverter {
    private ModelConverter() { }
    public static List<Parking> convertOpenDataNantesModelToParkBuddyMiageModel(ParkingSearchResult parkingsREST) {
        ArrayList<Parking> parkings = new ArrayList<>();
        for(ParkingSearchResult.ParkingResult itemREST : parkingsREST.getRecords()) {
            parkings.add(Parking.createParkingFromOpenDataNantesParking(itemREST.getFields()));
        }
        return parkings;
    }

    public static void addAvailabilityToParkBuddyMiageModel(List<Parking> parkings, List<AvailabilitySearchResult.AvailabilityResult> availabilities) {
        for(AvailabilitySearchResult.AvailabilityResult availability : availabilities) {
            for(Parking parking : parkings) {
                if(parking.id.get() == availability.getFields().getIdobj()) {
                    parking.updateParkingAvailabilityFrom(availability);
                }
            }
        }
    }

    public static void addScheduleToParkBuddyMiageModel(List<Parking> parkings, List<ScheduleSearchResult.ScheduleResult> schedules) {
        for(Parking parking : parkings)
            parking.clearSchedules();
        for(ScheduleSearchResult.ScheduleResult schedule : schedules) {
            for(Parking parking : parkings) {
                if(parking.id.get() == schedule.getFields().getIdobj()) {
                    parking.updateParkingScheduleFrom(schedule);
                }
            }
        }
    }
}
