package org.miage.parkbuddymiage.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableDouble;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;

import org.miage.parkbuddymiage.controller.LocationProviderService;
import org.miage.parkbuddymiage.model.opendatanantes.AvailabilitySearchResult;
import org.miage.parkbuddymiage.model.opendatanantes.ParkingSearchResult;
import org.miage.parkbuddymiage.model.opendatanantes.ScheduleSearchResult;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"id"})
@Accessors(prefix = "m")
@Entity(tableName = "Parking")
public class Parking {

    /////////////////////////
    // Identifiant         //
    /////////////////////////

    /**
     * Identifiant unique
     */
    @PrimaryKey
    public ObservableInt id = new ObservableInt();
    /**
     * Nom
     */
    public ObservableField<String> name = new ObservableField<>();
    /**
     * Description
     */
    public ObservableField<String> description = new ObservableField<>();
    /**
     * Accès transports en commun
     */
    public ObservableField<String> access = new ObservableField<>();
    /**
     * Site web de contact
     */
    public ObservableField<String> webSite = new ObservableField<>();
    /**
     * Téléphone de contact
     */
    public ObservableField<String> phoneNumber = new ObservableField<>();

    /////////////////////////
    // Localisation        //
    /////////////////////////

    /**
     * Adresse
     */
    public ObservableField<String> address = new ObservableField<>();
    /**
     * Code postal
     */
    public ObservableField<String> postCode = new ObservableField<>();
    /**
     * Ville
     */
    public ObservableField<String> city = new ObservableField<>();
    public String getPostCodeCity() {
        return postCode.get() + " " + city.get();
    }
    /**
     * Latitude
     */
    public ObservableDouble latitude = new ObservableDouble();
    /**
     * Longitude
     */
    public ObservableDouble longitude = new ObservableDouble();

    @Ignore
    @Getter
    private float mLastCurrentDistance = -1;

    /**
     * Retourne la distance par rapport à la dernière position connue
     */
    public float getCurrentDistanceFloat() {
        float d = LocationProviderService.INSTANCE().getCurrentDistance(latitude.get(), longitude.get());
        if(d != -1)
            mLastCurrentDistance = d;
        return mLastCurrentDistance;
    }

    /**
     * Retourne la distance par rapport à la dernière position connue
     */
    public String getCurrentDistance() {
        float d = LocationProviderService.INSTANCE().getCurrentDistance(latitude.get(), longitude.get());
        if(d != -1)
            mLastCurrentDistance = d;
        return LocationProviderService.INSTANCE().getCurrentDistanceString(mLastCurrentDistance);
    }

    /////////////////////////
    // Nombre de places    //
    /////////////////////////

    public enum AvailabilityStatus {
        OUT_OF_ORDER,
        CLOSED,
        SUBSCRIBERS,
        OPEN
    }

    /**
     * Statut du parking
     */
    public ObservableField<AvailabilityStatus> status = new ObservableField<>();
    /**
     * Nombre de places déclenchant l'affichage complet
     */
    public ObservableInt slotsDisplayFull = new ObservableInt(0);
    /**
     * Nombre de places disponible
     */
    public ObservableInt availableSlots = new ObservableInt();
    /**
     * Nombre de places total
     */
    public ObservableInt totalSlots = new ObservableInt();

    /**
     * Nombre de places pour véhicule électrique
     */
    public ObservableInt electricSlots = new ObservableInt();
    /**
     * @return Possibilité de garer des vélos
     */
    public boolean isElectricPark() {
        return electricSlots.get() > 0;
    }

    /**
     * Nombre de places pour personnes handicapées
     */
    public ObservableInt pmrSlots = new ObservableInt();

    /**
     * @return Possibilité de garer des vélos
     */
    public boolean isPmrPark() {
        return pmrSlots.get() > 0;
    }

    /**
     * Nombre de place pour les vélos
     */
    public ObservableInt bicycleSlots = new ObservableInt();

    /**
     * @return Possibilité de garer des vélos
     */
    public boolean isBicyclePark() {
        return bicycleSlots.get() > 0;
    }

    /**
     * Nombre de place pour les motos
     */
    public ObservableInt motorBikeSlots = new ObservableInt();

    /**
     * @return Possibilité de garer des motos
     */
    public boolean isMotorBikePark() {
        return motorBikeSlots.get() > 0;
    }

    /////////////////////////
    // Moyens de Paiement  //
    /////////////////////////

    /**
     * Possible de payer par carte bancaire ?
     */
    public ObservableBoolean ccPayment = new ObservableBoolean();
    /**
     * Possible de payer en espèce ?
     */
    public ObservableBoolean cashPayment = new ObservableBoolean();
    /**
     * Possible de payer par carte Total GR ?
     */
    public ObservableBoolean grPayment = new ObservableBoolean();

    /////////////////////////
    // Horaires            //
    /////////////////////////

    /**
     * Horaires
     */
    @Ignore
    private List<Schedule> mSchedule = new ArrayList<>();

    /**
     * @return Horaires
     */
    public List<Schedule> getSchedule() {
        return Collections.unmodifiableList(mSchedule);
    }

    void insertSchedule(Schedule schedule) {
        mSchedule.add(schedule);
    }
    void insertSchedules(Collection<? extends Schedule> schedules) {
        mSchedule.addAll(schedules);
    }
    void clearSchedules() {
        mSchedule.clear();
    }

    /**
     * @return Des horaires sont-elles disponibles ?
     */
    public boolean canGetSchedule() {
        for(DayOfWeek day : DayOfWeek.values())
            if(canGetSchedule(day))
                return true;
        return false;
    }

    /**
     * @return Les horaires du jour X sont-elle disponibles ?
     */
    public boolean canGetSchedule(DayOfWeek day) {
        return !getSchedule(day).isEmpty();
    }

    public String getSchedule(DayOfWeek day) {
        StringBuilder result = new StringBuilder();
        List<TupleSchedule> schedulesOpened = new ArrayList<>();
        List<TupleSchedule> schedulesClosed = new ArrayList<>();
        for(Schedule schedule : getSchedule()) {
            if(schedule.getDay() == day && schedule.getValidityStartDate().isBefore(LocalDate.now())
                                        && schedule.getValidityEndDate().isAfter(LocalDate.now())) {
                TupleSchedule tuple = new TupleSchedule(schedule.getStartTime(), schedule.getEndTime());
                if(schedule.isOpening())
                    schedulesOpened.add(tuple);
                else
                    schedulesClosed.add(tuple);
            }
        }
        for(TupleSchedule tuple : schedulesClosed) {
            List<TupleSchedule> tmpSchedules = new ArrayList<>();
            for(TupleSchedule tupleBis : schedulesOpened) {
                if(isTimeBetween(tuple.start, tupleBis)) {
                    if(isTimeBetween(tuple.end, tupleBis)) {
                        TupleSchedule newTuple = new TupleSchedule(tuple.end, tupleBis.end);
                        tmpSchedules.add(newTuple);
                        tupleBis.end = tuple.start;
                    } else {
                        tupleBis.end = tuple.start;
                    }
                }
                else if(isTimeBetween(tuple.end, tupleBis)) {
                    tupleBis.start = tuple.end;
                }
                tmpSchedules.add(tupleBis);
            }
            schedulesOpened = tmpSchedules;
        }
        Collections.sort(schedulesOpened, new Comparator<TupleSchedule>() {
            @Override
            public int compare(TupleSchedule o1, TupleSchedule o2) {
                if(o1.start.equals(o2.start))
                    return o1.start.compareTo(o2.start);
                return o1.end.compareTo(o2.end);
            }
        });
        for(TupleSchedule schedule : schedulesOpened) {
            if(result.length() != 0)
                result.append("\n");
            result.append(getLocalTimeAsString(schedule.start));
            result.append("-");
            result.append(getLocalTimeAsString(schedule.end));
        }
        return result.toString();
    }

    private String getLocalTimeAsString(LocalTime time) {
        DateTimeFormatter dtf;
        if(time.getMinute() == 0)
            dtf = DateTimeFormatter.ofPattern("H'h'");
        else
            dtf = DateTimeFormatter.ofPattern("H'h'm");
        return time.format(dtf);
    }

    private boolean isTimeBetween(LocalTime theOne, TupleSchedule theTuple) {
        return theTuple.start.isBefore(theOne) && theTuple.end.isAfter(theOne);
    }

    /////////////////////////
    // Utilisateur         //
    /////////////////////////

    /**
     * Favoris de l'utilisation ?
     */
    public ObservableBoolean favorite = new ObservableBoolean();

    /////////////////////////
    // Conversion          //
    /////////////////////////

    static Parking createParkingFromOpenDataNantesParking(ParkingSearchResult.ParkingResult.Fields parkingODN) {
        Parking parking = new Parking();
        parking.updateParkingFrom(parkingODN);
        return parking;
    }

    private void updateParkingFrom(ParkingSearchResult.ParkingResult.Fields parkingODN) {
        this.id.set(parkingODN.getIdobj());
        this.name.set(parkingODN.getNom_complet());
        this.description.set(parkingODN.getPresentation());
        this.access.set(parkingODN.getAcces_transports_communs());
        this.webSite.set(parkingODN.getSite_web());
        this.phoneNumber.set(parkingODN.getTelephone());
        this.address.set(parkingODN.getAdresse());
        this.postCode.set(parkingODN.getCode_postal());
        this.city.set(parkingODN.getCommune());
        this.latitude.set(parkingODN.getLatitude());
        this.longitude.set(parkingODN.getLongitude());
        this.totalSlots.set(parkingODN.getCapacite_voiture());
        this.electricSlots.set(parkingODN.getCapacite_vehicule_electrique());
        this.pmrSlots.set(parkingODN.getCapacite_pmr());
        this.motorBikeSlots.set(parkingODN.getCapacite_moto());
        this.bicycleSlots.set(parkingODN.getCapacite_velo());
        this.ccPayment.set(parkingODN.getCCPayment());
        this.cashPayment.set(parkingODN.getCashPayment());
        this.grPayment.set(parkingODN.getGRPayment());
    }

    void updateParkingAvailabilityFrom(AvailabilitySearchResult.AvailabilityResult result) {
        this.status.set(result.getFields().getStatus());
        this.slotsDisplayFull.set(result.getFields().getGrp_complet());
        this.availableSlots.set(result.getFields().getGrp_disponible());
    }

    void updateParkingScheduleFrom(ScheduleSearchResult.ScheduleResult result) {
        ScheduleSearchResult.ScheduleResult.Fields fields = result.getFields();
        Schedule schedule = new Schedule();
        schedule.setParkingId(fields.getIdobj());
        schedule.setDay(fields.getDay());
        schedule.setOpening(fields.isOuverture());
        schedule.setStartTime(fields.getStartTime());
        schedule.setEndTime(fields.getEndTime());
        schedule.setValidityStartDate(fields.getStartDate());
        schedule.setValidityEndDate(fields.getEndDate());
        mSchedule.add(schedule);
    }

    @AllArgsConstructor
    private class TupleSchedule {
        LocalTime start;
        LocalTime end;
    }
}
