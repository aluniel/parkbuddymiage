package org.miage.parkbuddymiage.model.opendatanantes;

import com.google.gson.annotations.Expose;

import java.util.List;

import lombok.Getter;
import lombok.ToString;

@ToString
public class ParkingSearchResult {
    /**
     * Nombre total de lignes
     */
    @Expose @Getter
    int nhits;

    public int getNextStart() {
        return parameters.start + parameters.rows;
    }

    /**
     * Paramètres de la réponse
     */
    @Expose @Getter
    Parameters parameters;
    @ToString
    public class Parameters {
        /**
         * Nombre de lignes affichées
         */
        @Expose @Getter
        int rows;
        /**
         * Ligne de départ
         */
        @Expose @Getter
        int start;
    }

    /**
     * Liste des enregistrements renvoyés
     */
    @Expose @Getter
    List<ParkingResult> records;
    @ToString
    public class ParkingResult {
        /**
         * Identifiant de l'enregistrement
         */
        @Expose @Getter
        String recordid;

        /**
         * Champs de l'enregistrement
         */
        @Expose @Getter
        Fields fields;
        @ToString
        public class Fields {
            /**
             * Identifiant du parking
             */
            @Expose @Getter
            int idobj;
            /**
             * Nom du parking
             */
            @Expose @Getter
            String nom_complet;
            /**
             * Description
             */
            @Expose @Getter
            String presentation;
            /**
             * Accès transport en commun
             */
            @Expose @Getter
            String acces_transports_communs;
            /**
             * Site web de contact
             */
            @Expose @Getter
            String site_web;
            /**
             * Téléphone de contact
             */
            @Expose @Getter
            String telephone;
            /**
             * Adresse
             */
            @Expose @Getter
            String adresse;
            /**
             * Code postal
             */
            @Expose @Getter
            String code_postal;
            /**
             * Ville
             */
            @Expose @Getter
            String commune;
            /**
             * Capacité voitures
             */
            @Expose @Getter
            int capacite_voiture;
            /**
             * Capacité voitures électriques
             */
            @Expose @Getter
            int capacite_vehicule_electrique;
            /**
             * Capacité PMR
             */
            @Expose @Getter
            int capacite_pmr;
            /**
             * Capacité motos
             */
            @Expose @Getter
            int capacite_moto;
            /**
             * Capacité vélos
             */
            @Expose @Getter
            int capacite_velo;
            /**
             * Coordonnées GPS
             */
            @Expose
            List<Double> location;
            public double getLatitude() {
                return location.size() >= 2 && location.get(0) != null ? location.get(0) : 0d;
            }
            public double getLongitude() {
                return location.size() >= 2 && location.get(1) != null ? location.get(1) : 0d;
            }
            /**
             * Moyens de paiement
             */
            @Expose
            String moyen_paiement;
            public boolean getCCPayment() {
                return moyen_paiement != null && moyen_paiement.contains("CB en borne de sortie");
            }
            public boolean getCashPayment() {
                return moyen_paiement != null && moyen_paiement.contains("Espèces");
            }
            public boolean getGRPayment() {
                return moyen_paiement != null && moyen_paiement.contains("Total GR");
            }
        }
    }
}
