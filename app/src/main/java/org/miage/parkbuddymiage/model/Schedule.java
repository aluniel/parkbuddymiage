package org.miage.parkbuddymiage.model;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@NoArgsConstructor
@ToString
@Accessors(prefix = "m")
@Entity(tableName = "Schedule")
public class Schedule {
    /**
     * Identifiant unique pour Room
     */
    @PrimaryKey(autoGenerate = true)
    @Getter @Setter
    private int mRoomId;
    /**
     * Identifiant du parking
     */
    @Getter @Setter
    private int mParkingId;
    /**
     * Début de la date de validité
     */
    @Getter @Setter
    private LocalDate mValidityStartDate;
    /**
     * Début de la date de validité
     */
    @Getter @Setter
    private LocalDate mValidityEndDate;
    /**
     * Jour de la semaine
     */
    @Getter @Setter
    private DayOfWeek mDay;
    /**
     * Heure de début
     */
    @Getter @Setter
    private LocalTime mStartTime;
    /**
     * Heure de fin
     */
    @Getter @Setter
    private LocalTime mEndTime;
    /**
     * Ouverture (true) ou fermeture (false)
     */
    @Getter @Setter
    private boolean mOpening;
}
