package org.miage.parkbuddymiage.model.opendatanantes;

import com.google.gson.annotations.Expose;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Objects;

import lombok.Getter;
import lombok.ToString;

@ToString
public class ScheduleSearchResult {

    /**
     * Nombre total de lignes
     */
    @Expose
    @Getter
    int nhits;

    public int getNextStart() {
        return parameters.start + parameters.rows;
    }

    /**
     * Paramètres de la réponse
     */
    @Expose @Getter
    AvailabilitySearchResult.Parameters parameters;
    @ToString
    public class Parameters {
        /**
         * Nombre de lignes affichées
         */
        @Expose @Getter
        int rows;
        /**
         * Ligne de départ
         */
        @Expose @Getter
        int start;
    }

    /**
     * Liste des enregistrements renvoyés
     */
    @Expose @Getter
    List<ScheduleResult> records;
    @ToString
    public class ScheduleResult {
        /**
         * Identifiant de l'enregistrement
         */
        @Expose @Getter
        String recordid;

        /**
         * Champs de l'enregistrement
         */
        @Expose @Getter
        Fields fields;
        @ToString
        public class Fields {
            /**
             * Identifiant du parking
             */
            @Expose @Getter
            int idobj;
            /**
             * Ouverture ou fermeture
             */
            @Expose
            String type_horaire;
            public boolean isOuverture() {
                return Objects.equals(type_horaire, "Ouverture");
            }
            /**
             * Jour de la semaine
             */
            @Expose
            public String jour;
            public DayOfWeek getDay() {
                switch(jour) {
                    case "lundi":
                        return DayOfWeek.MONDAY;
                    case "mardi":
                        return DayOfWeek.TUESDAY;
                    case "mercredi":
                        return DayOfWeek.WEDNESDAY;
                    case "jeudi":
                        return DayOfWeek.THURSDAY;
                    case "vendredi":
                        return DayOfWeek.FRIDAY;
                    case "samedi":
                        return DayOfWeek.SATURDAY;
                    case "dimanche":
                        return DayOfWeek.SUNDAY;
                    default:
                        return null;
                }
            }
            /**
             * Heure de début
             */
            @Expose
            public String heure_debut;
            public LocalTime getStartTime() {
                try {
                    return LocalTime.of(Integer.parseInt(heure_debut.split(":")[0]), Integer.parseInt(heure_debut.split(":")[1]));
                } catch(Exception ex) {
                    return null;
                }
            }
            /**
             * Heure de fin
             */
            @Expose
            public String heure_fin;
            public LocalTime getEndTime() {
                try {
                    return LocalTime.of(Integer.parseInt(heure_fin.split(":")[0]), Integer.parseInt(heure_fin.split(":")[1]));
                } catch (Exception ex) {
                    return null;
                }
            }
            /**
             * Date de début de validité
             */
            @Expose
            public String date_debut;
            public LocalDate getStartDate() {
                try {
                    return LocalDate.parse(date_debut);
                } catch (Exception ex) {
                    return null;
                }
            }
            /**
             * Date de fin de validité
             */
            @Expose
            public String date_fin;
            public LocalDate getEndDate() {
                try {
                    return LocalDate.parse(date_fin);
                } catch (Exception ex) {
                    return null;
                }
            }
        }
    }
}
