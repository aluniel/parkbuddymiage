package org.miage.parkbuddymiage.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import org.miage.parkbuddymiage.controller.SearchInStringEngine;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Dao
public abstract class ParkingDao {

    /////////////////////////
    // Parkings/schedules  //
    /////////////////////////

    /**
     * @return Liste des parkings selon les critères de recherche
     */
    public List<Parking> getAll(boolean favOnly, String filter, String nameFilter, String addressFilter, int capacityFilter, boolean ccPaymentFilter, boolean cashPaymentFilter, boolean grPaymentFilter) {
        List<Parking> parkings = getAll(favOnly, filter);
        if(!nameFilter.isEmpty()) {
            List<Parking> tmpParkings = parkings;
            parkings = new ArrayList<>();
            for(Parking parking : tmpParkings)
                if(SearchInStringEngine.searchInString(parking.name.get(), nameFilter))
                    parkings.add(parking);
        }
        if(!addressFilter.isEmpty()) {
            List<Parking> tmpParkings = parkings;
            parkings = new ArrayList<>();
            for(Parking parking : tmpParkings)
                if(SearchInStringEngine.searchInString(parking.address.get(), addressFilter))
                    parkings.add(parking);
        }
        if(capacityFilter > 0) {
            List<Parking> tmpParkings = parkings;
            parkings = new ArrayList<>();
            for(Parking parking : tmpParkings)
                if(parking.availableSlots.get() >= capacityFilter)
                    parkings.add(parking);
        }
        if(ccPaymentFilter) {
            List<Parking> tmpParkings = parkings;
            parkings = new ArrayList<>();
            for(Parking parking : tmpParkings)
                if(parking.ccPayment.get())
                    parkings.add(parking);
        }
        if(cashPaymentFilter) {
            List<Parking> tmpParkings = parkings;
            parkings = new ArrayList<>();
            for(Parking parking : tmpParkings)
                if(parking.cashPayment.get())
                    parkings.add(parking);
        }
        if(cashPaymentFilter) {
            List<Parking> tmpParkings = parkings;
            parkings = new ArrayList<>();
            for(Parking parking : tmpParkings)
                if(parking.grPayment.get())
                    parkings.add(parking);
        }
        return parkings;
    }

    /**
     * @return Liste des parkings selon critères de recherche rapide
     */
    public List<Parking> getAll(boolean favOnly, String filter) {
        List<Parking> parkings = getAll();
        if(!filter.isEmpty()) {
            List<Parking> tmpParkings = parkings;
            parkings = new ArrayList<>();
            for(Parking parking : tmpParkings) {
                if(SearchInStringEngine.searchInString(parking.name.get(), filter))
                    parkings.add(parking);
                else if(SearchInStringEngine.searchInString(parking.address.get(), filter))
                    parkings.add(parking);
                else if(SearchInStringEngine.searchInString(parking.getPostCodeCity(), filter))
                    parkings.add(parking);
            }
        }
        if(favOnly) {
            List<Parking> tmpParkings = new ArrayList<>();
            for(Parking parking : parkings) {
                if(parking.favorite.get())
                    tmpParkings.add(parking);
            }
            parkings = tmpParkings;
        }
        return parkings;
    }

    /**
     * @return Liste des parkings
     */
    public List<Parking> getAll() {
        List<Parking>  parkings  = getAllParkings();
        List<Schedule> schedules = getAllSchedules();
        for(Parking parking : parkings) {
            for(Iterator<Schedule> iterator = schedules.listIterator() ; iterator.hasNext() ; ) {
                Schedule schedule = iterator.next();
                if(schedule.getParkingId() == parking.id.get()) {
                    parking.insertSchedule(schedule);
                    iterator.remove();
                }
            }
        }
        return parkings;
    }

    public Parking getParking(int id) {
        Parking parking = getParkingWithoutSchedule(id);
        List<Schedule> schedules = getSchedules(id);
        parking.insertSchedules(schedules);
        return parking;
    }

    /**
     * Enregistre les parking en BD
     */
    @Transaction
    public void update(List<Parking> parkings) {
        deleteAllParkings();
        deleteAllSchedules();
        List<Schedule> schedules = new ArrayList<>();
        for(Parking parking : parkings)
            schedules.addAll(parking.getSchedule());
        insertParkings(parkings);
        insertSchedules(schedules);
    }

    /////////////////////////
    // Parkings            //
    /////////////////////////

    @Query("SELECT * FROM Parking")
    abstract List<Parking> getAllParkings();

    @Query("SELECT * FROM Parking WHERE id = :id")
    abstract Parking getParkingWithoutSchedule(int id);

    @Insert
    abstract void insertParkings(List<Parking> parkings);

    @Query("SELECT favorite FROM Parking WHERE id = :id")
    public abstract boolean isParkingFavorite(int id);

    @Query("UPDATE Parking SET favorite = :fav WHERE id = :id")
    public abstract void updateFavorite(int id, boolean fav);

    @Query("DELETE FROM Parking")
    abstract void deleteAllParkings();

    /////////////////////////
    // Schedules           //
    /////////////////////////

    @Query("SELECT * FROM Schedule")
    abstract List<Schedule> getAllSchedules();

    @Query("SELECT * FROM Schedule WHERE mParkingId = :idParking")
    abstract List<Schedule> getSchedules(int idParking);

    @Insert
    abstract void insertSchedules(List<Schedule> schedules);

    @Query("DELETE FROM Schedule")
    abstract void deleteAllSchedules();

    /////////////////////////
    // Settings            //
    /////////////////////////

    /**
     * Enregistre les settings en BD
     */
    void updateSettings() {
        insertSettings(Settings.INSTANCE);
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract void insertSettings(Settings... settings);

    /**
     * Charge les settings
     */
    @Query("SELECT * FROM Settings")
    public abstract List<Settings> getSettings();
}
