package org.miage.parkbuddymiage.view;

import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;

import org.miage.parkbuddymiage.R;
import org.miage.parkbuddymiage.controller.ParkingManager;
import org.miage.parkbuddymiage.event.BusManager;
import org.miage.parkbuddymiage.event.ShowScreenEvent;
import org.miage.parkbuddymiage.databinding.FragmentParkingBinding;
import org.miage.parkbuddymiage.model.Parking;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

/**
 * Created by alexa on 20/11/2018.
 */

public class ParkingFragment extends BaseFragment implements OnStreetViewPanoramaReadyCallback {

    @BindView(R.id.fragment_parking_scroll)
    ScrollView mScrollView;

    @BindView(R.id.fragment_parking_website)
    TextView mWebSite;

    @BindView(R.id.fragment_parking_fav)
    ImageButton mFavButton;

    @BindView(R.id.fragment_parking_map)
    Button mMapButton;

    private Parking mParking;

    private AsyncTask<Void, Void, Parking> mTask;

    public static ParkingFragment newInstance(int id) {
        Bundle args = new Bundle();
        args.putInt("parking", id);

        ParkingFragment fragment = new ParkingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTask = ParkingManager.getParkingsFromLocalDB(getArguments().getInt("parking"));

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            mParking = mTask.get();
        } catch (Exception e) {
            e.printStackTrace();
            mParking = new Parking();
        }
        FragmentParkingBinding binding = DataBindingUtil.inflate(inflater, getLayout(), container, false);
        View view = binding.getRoot();
        binding.setParking(mParking);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mWebSite.setMovementMethod(LinkMovementMethod.getInstance());
        SupportStreetViewPanoramaFragment streetViewPanoramaFragment = (SupportStreetViewPanoramaFragment)getChildFragmentManager().findFragmentById(R.id.fragment_parking_street_view);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(this);
    }

    @OnClick(R.id.fragment_parking_fav)
    public void onFavClick() {
        mParking.favorite.set(!mParking.favorite.get());
        ParkingManager.setFavorite(mParking.id.get(), mParking.favorite.get());
    }

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
        streetViewPanorama.setUserNavigationEnabled(false);
        streetViewPanorama.setPosition(new LatLng(mParking.latitude.get(), mParking.longitude.get()));
    }

    @OnTouch(R.id.fragment_parking_street_view_bidouille)
    public boolean onStreetViewTouched(View view, MotionEvent event) {
        int action = event.getAction();
        switch(action) {
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_DOWN:
                mScrollView.requestDisallowInterceptTouchEvent(true);
                return false;
            case MotionEvent.ACTION_UP:
                mScrollView.requestDisallowInterceptTouchEvent(false);
                return true;
            default:
                return true;
        }
    }

    @OnClick(R.id.fragment_parking_map)
    public void onDisplayOnMapClick() {
        ParkingManager.activateSearch(mParking.name.get(), mParking.address.get(), 0, false, false, false);
        BusManager.BUS.post(ShowScreenEvent.Screen(ShowScreenEvent.Screen.Map));
    }
}
