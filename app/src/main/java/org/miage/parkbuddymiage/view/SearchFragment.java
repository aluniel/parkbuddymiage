package org.miage.parkbuddymiage.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import org.miage.parkbuddymiage.R;
import org.miage.parkbuddymiage.controller.ParkingManager;
import org.miage.parkbuddymiage.event.BusManager;
import org.miage.parkbuddymiage.event.ShowScreenEvent;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by alexa on 20/11/2018.
 */

public class SearchFragment extends BaseFragment {
    public static SearchFragment newInstance() {
        return new SearchFragment();
    }

    @BindView(R.id.fragment_search_name)
    EditText mName;

    @BindView(R.id.fragment_search_address)
    EditText mAddress;

    @BindView(R.id.fragment_search_capacity)
    SeekBar mCapacity;

    @BindView(R.id.fragment_search_count_capacity)
    TextView mCountCapacity;

    @BindView(R.id.fragment_search_ccpayment)
    CheckBox mCCPayment;

    @BindView(R.id.fragment_search_cashpayment)
    CheckBox mCashPayment;

    @BindView(R.id.fragment_search_grpayment)
    CheckBox mGRPayment;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCapacity.setOnSeekBarChangeListener(onSlide);
    }

    private SeekBar.OnSeekBarChangeListener onSlide = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if(progress == 0) {
                mCountCapacity.setText(R.string.peu_importe);
            } else {
                mCountCapacity.setText(Integer.toString(progress));
            }
        }
        @Override public void onStartTrackingTouch(SeekBar seekBar) { }
        @Override public void onStopTrackingTouch(SeekBar seekBar) { }
    };

    @OnClick(R.id.fragment_search_list)
    @SuppressWarnings("unused")
    public void onListClick() {
        loadSearchSettingsIntoParkingManager();
        BusManager.BUS.post(ShowScreenEvent.Screen(ShowScreenEvent.Screen.List));
    }

    @OnClick(R.id.fragment_search_map)
    @SuppressWarnings("unused")
    public void onMapClick() {
        loadSearchSettingsIntoParkingManager();
        BusManager.BUS.post(ShowScreenEvent.Screen(ShowScreenEvent.Screen.Map));
    }

    private void loadSearchSettingsIntoParkingManager() {
        ParkingManager.activateSearch(mName.getText().toString(), mAddress.getText().toString(), mCapacity.getProgress(), mCCPayment.isChecked(), mCashPayment.isChecked(), mGRPayment.isChecked());
    }
}
