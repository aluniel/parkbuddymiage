package org.miage.parkbuddymiage.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.otto.Subscribe;

import org.miage.parkbuddymiage.R;
import org.miage.parkbuddymiage.controller.ParkingManager;
import org.miage.parkbuddymiage.event.BusManager;
import org.miage.parkbuddymiage.event.ParkingUpdatedEvent;
import org.miage.parkbuddymiage.event.ShowScreenEvent;
import org.miage.parkbuddymiage.model.Parking;

import java.util.HashMap;
import java.util.Objects;

import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by alexa on 20/11/2018.
 */

public class MapFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private HashMap<String,Parking> listParking;

    public static MapFragment newInstance() {
        return new MapFragment();
    }

    private GoogleMap map;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.listParking = new HashMap<>();

        //Get Map Fragment
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        ParkingManager.getParkingsFromLocalDB();
        map.setOnInfoWindowClickListener(this);
        if(!EasyPermissions.hasPermissions(Objects.requireNonNull(getContext()), Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION))
            EasyPermissions.requestPermissions(this, "Localisation", 1, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION);
        if(!EasyPermissions.hasPermissions(Objects.requireNonNull(getContext()), Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION))
            return;
        map.setMyLocationEnabled(true);
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void onParkingFound(final ParkingUpdatedEvent event) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (map != null) {
                    // Update map's markers
                    map.clear();
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    for (Parking parking : event.getParkings()) {
                        // Step 1: create marker icon (and resize drawable so that marker is not too big)

                        int markerIconResource;

                        // Step 2: define marker options
                        MarkerOptions markerOptions = new MarkerOptions()
                                .position(new LatLng(parking.latitude.get(), parking.longitude.get()))
                                .title(parking.name.get())
                                .snippet(parking.address.get() + ", " + parking.city.get()+ ", places : " + parking.availableSlots.get());
                        // If open parking
                        if(parking.status.get() == Parking.AvailabilityStatus.OPEN){
                            if(parking.favorite.get())
                                markerOptions.icon(vectorToBitmap(R.drawable.ic_map_marker_green_fav));
                            else
                                markerOptions.icon(vectorToBitmap(R.drawable.ic_map_marker_green));
                        }
                        //If close or full
                        else{
                            if(parking.favorite.get())
                                markerOptions.icon(vectorToBitmap(R.drawable.ic_map_marker_red_fav));
                            else
                                markerOptions.icon(vectorToBitmap(R.drawable.ic_map_marker_red));
                        }
                        // Step 3: add marker
                        map.addMarker(markerOptions);
                        builder.include(markerOptions.getPosition());
                        listParking.put(markerOptions.getTitle(),parking);
                    }
                    if(event.getParkings().size() > 0) {
                        LatLngBounds bounds = builder.build();

                        int width = getResources().getDisplayMetrics().widthPixels;
                        int height = getResources().getDisplayMetrics().heightPixels;
                        int padding = (int) (width * 0.25); // offset from edges of the map 10% of screen

                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

                        map.animateCamera(cu);
                    }
                }
            }
        });
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        BusManager.BUS.post(ShowScreenEvent.Parking(listParking.get(marker.getTitle())));
    }

    private BitmapDescriptor vectorToBitmap(@DrawableRes int id) {
        Drawable vectorDrawable = ResourcesCompat.getDrawable(getResources(), id, null);
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        int width  = 71;
        int height = 120;
        Bitmap smallMarker = Bitmap.createScaledBitmap(bitmap, width, height, false);
        return BitmapDescriptorFactory.fromBitmap(smallMarker);
    }
}
