package org.miage.parkbuddymiage.view;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import org.miage.parkbuddymiage.R;
import org.miage.parkbuddymiage.controller.ParkingManager;
import org.miage.parkbuddymiage.event.BusManager;
import org.miage.parkbuddymiage.event.DisplayToastEvent;
import org.miage.parkbuddymiage.event.NotifyUserEvent;
import org.miage.parkbuddymiage.event.ShowScreenEvent;
import org.miage.parkbuddymiage.event.BackgroundTaskUpdatedEvent;
import org.miage.parkbuddymiage.model.Parking;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by alexa on 14/11/2018.
 */

public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.navigationBar)
    protected BottomNavigationView mNavigationBar;

    @BindView(R.id.progress_bar)
    protected ProgressBar mProgressBar;

    protected Unbinder mUnbinder;

    protected Fragment mSelectedFragment;

    protected Fragment mHomeFragment;
    protected DataViewFragment mDataView;
    protected Fragment mSearchFragment;
    protected Fragment mSettingsFragment;
    protected Fragment mParkingFragment;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    showScreen(ShowScreenEvent.Screen.Home);
                    break;
                case R.id.navigation_list:
                    showScreen(ShowScreenEvent.Screen.List);
                    break;
                case R.id.navigation_map:
                    showScreen(ShowScreenEvent.Screen.Map);
                    break;
                case R.id.navigation_search:
                    showScreen(ShowScreenEvent.Screen.Search);
                    break;
                case R.id.navigation_settings:
                    showScreen(ShowScreenEvent.Screen.Settings);
                    break;
                default:
                    return false;
            }
            return true;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        mUnbinder = ButterKnife.bind(this);
        mHomeFragment = HomeFragment.newInstance();
        mNavigationBar.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        showScreen(ShowScreenEvent.Screen.Home);
        BusManager.BUS.register(this);
    }

    @Subscribe
    public void onNotifyUserEvent(NotifyUserEvent event) {
        Intent intent = new Intent(getApplicationContext(), BaseActivity.class);
        intent.putExtra("toDataParkingFragment", "true");
        intent.putExtra("idParking", event.parking.id.get());
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext(), "25")
                        .setSmallIcon(R.drawable.ic_application)
                        .setContentTitle("Un parking n'est pas loin ! (500m) ")
                        .setContentText("Il reste " + event.parking.availableSlots.get() + " places dans " + event.parking.name.get())
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setContentIntent(pendingIntent)
                        .setDefaults(Notification.DEFAULT_SOUND|Notification.DEFAULT_LIGHTS|Notification.DEFAULT_VIBRATE)
                        .setAutoCancel(true);
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder.setAutoCancel(true);
        NotificationManager mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, mBuilder.build());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
        BusManager.BUS.unregister(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String isNotificationClic = getIntent().getStringExtra("toDataParkingFragment");
        int id_parking = getIntent().getIntExtra("idParking", -1);
        if(isNotificationClic != null && isNotificationClic.equals("true") && id_parking != -1) {
            showScreen(ShowScreenEvent.Parking(id_parking));
            getIntent().putExtra("toDataParkingFragment", "false");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Subscribe
    public void onBackgroundTaskUpdated(final BackgroundTaskUpdatedEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressBar.setVisibility(event.taskBeginning ? View.VISIBLE : View.INVISIBLE);
            }
        });
    }

    private void showScreen(ShowScreenEvent.Screen screen) {
        showScreen(ShowScreenEvent.Screen(screen));
    }

    @Subscribe
    public void showScreen(ShowScreenEvent event) {
        ShowScreenEvent.Screen screen = event.getScreen();
        try {
            mNavigationBar.setOnNavigationItemSelectedListener(null);
            boolean addToBackStack = true;
            switch (screen) {
                case Home:
                    mNavigationBar.setSelectedItemId(R.id.navigation_home);
                    if(mSelectedFragment == mHomeFragment)
                        return;
                    if(mHomeFragment == null)
                        mHomeFragment = HomeFragment.newInstance();
                    mSelectedFragment = mHomeFragment;
                    break;
                case List:
                    mNavigationBar.setSelectedItemId(R.id.navigation_list);
                    if(mSelectedFragment == mDataView && mDataView.getScreen() == DataViewFragment.DataScreen.List)
                        return;
                    if(mDataView == null)
                        mDataView = DataViewFragment.newInstance();
                    mDataView.setScreen(DataViewFragment.DataScreen.List);
                    mSelectedFragment = mDataView;
                    addToBackStack = false;
                    break;
                case Map:
                    mNavigationBar.setSelectedItemId(R.id.navigation_map);
                    if(mSelectedFragment == mDataView && mDataView.getScreen() == DataViewFragment.DataScreen.Map)
                        return;
                    if(mDataView == null)
                        mDataView = DataViewFragment.newInstance();
                    mDataView.setScreen(DataViewFragment.DataScreen.Map);
                    mSelectedFragment = mDataView;
                    addToBackStack = false;
                    break;
                case Parking:
                    if(mSelectedFragment == mParkingFragment)
                        return;
                    mParkingFragment = ParkingFragment.newInstance(event.getParking());
                    mSelectedFragment = mParkingFragment;
                    break;
                case Search:
                    mNavigationBar.setSelectedItemId(R.id.navigation_search);
                    if(mSelectedFragment == mSearchFragment)
                        return;
                    if(mSearchFragment == null)
                        mSearchFragment = SearchFragment.newInstance();
                    mSelectedFragment = mSearchFragment;
                    break;
                case Settings:
                    mNavigationBar.setSelectedItemId(R.id.navigation_settings);
                    if(mSelectedFragment == mSettingsFragment)
                        return;
                    if(mSettingsFragment == null)
                        mSettingsFragment = SettingsFragment.newInstance();
                    mSelectedFragment = mSettingsFragment;
                    break;
                default:
                    return;
            }
            if(addToBackStack)
                getSupportFragmentManager().beginTransaction().replace(R.id.activity_container, mSelectedFragment).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).addToBackStack(null).commit();
            else
                getSupportFragmentManager().beginTransaction().replace(R.id.activity_container, mSelectedFragment).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
        }
        finally {
            mNavigationBar.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        }
    }

    @Subscribe
    public void showToast(final DisplayToastEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), event.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        // if there is a fragment and the back stack of this fragment is not empty,
        // then emulate 'onBackPressed' behaviour, because in default, it is not working
        FragmentManager fm = getSupportFragmentManager();
        for (Fragment frag : fm.getFragments()) {
            if (frag.isVisible()) {
                FragmentManager childFm = frag.getChildFragmentManager();
                if (childFm.getBackStackEntryCount() > 0) {
                    childFm.popBackStack();
                    return;
                }
            }
        }
        super.onBackPressed();
    }
}
