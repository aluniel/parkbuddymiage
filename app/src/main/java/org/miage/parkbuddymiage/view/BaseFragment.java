package org.miage.parkbuddymiage.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.miage.parkbuddymiage.R;
import org.miage.parkbuddymiage.event.BusManager;
import org.miage.parkbuddymiage.event.ShowScreenEvent;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment {

    protected BaseActivity mActivity;

    protected ShowScreenEvent.Screen mScreen;

    protected Unbinder mUnbinder;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(this instanceof HomeFragment)
            mScreen = ShowScreenEvent.Screen.Home;
        if(this instanceof ListFragment)
            mScreen = ShowScreenEvent.Screen.List;
        if(this instanceof MapFragment)
            mScreen = ShowScreenEvent.Screen.Map;
        if(this instanceof SearchFragment)
            mScreen = ShowScreenEvent.Screen.Search;
        if(this instanceof SettingsFragment)
            mScreen = ShowScreenEvent.Screen.Settings;
        if(this instanceof ParkingFragment)
            mScreen = ShowScreenEvent.Screen.Parking;
        if(this instanceof DataViewFragment)
            mScreen = null;
        if(context instanceof BaseActivity) {
            mActivity = (BaseActivity)context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        BusManager.BUS.register(this);
        if(mScreen != null && mScreen != ShowScreenEvent.Screen.Parking && mScreen != ShowScreenEvent.Screen.Home)
            BusManager.BUS.post(ShowScreenEvent.Screen(mScreen));
    }

    @Override
    public void onPause() {
        super.onPause();
        BusManager.BUS.unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    protected int getLayout() {
        if(this instanceof HomeFragment)
            return R.layout.fragment_home;
        if(this instanceof ListFragment)
            return R.layout.fragment_list;
        if(this instanceof MapFragment)
            return R.layout.fragment_maps;
        if(this instanceof SearchFragment)
            return R.layout.fragment_search;
        if(this instanceof SettingsFragment)
            return R.layout.fragment_settings;
        if(this instanceof ParkingFragment)
            return R.layout.fragment_parking;
        if(this instanceof DataViewFragment)
            return R.layout.fragment_data_view;
        return 0;
    }

    protected int getNavigationItem() {
        if(this instanceof HomeFragment)
            return R.id.navigation_home;
        if(this instanceof ListFragment)
            return R.id.navigation_list;
        if(this instanceof MapFragment)
            return R.id.navigation_map;
        if(this instanceof SearchFragment)
            return R.id.navigation_search;
        if(this instanceof SettingsFragment)
            return R.id.navigation_settings;
        return 0;
    }
}
