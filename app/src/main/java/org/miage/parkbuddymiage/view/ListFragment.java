package org.miage.parkbuddymiage.view;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.squareup.otto.Subscribe;

import org.miage.parkbuddymiage.R;
import org.miage.parkbuddymiage.controller.ParkingManager;
import org.miage.parkbuddymiage.event.BusManager;
import org.miage.parkbuddymiage.controller.LocationProviderService;
import org.miage.parkbuddymiage.event.DistantQueryFinishedEvent;
import org.miage.parkbuddymiage.event.ParkingUpdatedEvent;

import java.util.Objects;

import butterknife.BindView;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by alexa on 20/11/2018.
 */

public class ListFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    public static ListFragment newInstance() {
        return new ListFragment();
    }

    @BindView(R.id.activity_list_listview)
    RecyclerView mParkingListView;

    @BindView(R.id.activity_list_swipe_refresh)
    SwipeRefreshLayout mSwipeRefresh;

    private ParkingAdapter mParkingAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mParkingAdapter = new ParkingAdapter(getActivity());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mParkingListView.setAdapter(mParkingAdapter);
        mParkingListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        DividerItemDecoration itemDecoration = new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL);
        itemDecoration.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(view.getContext(), R.drawable.shape_divider_transparent)));
        mParkingListView.addItemDecoration(itemDecoration);
        mParkingListView.setItemAnimator(new SlideInUpAnimator());

        mSwipeRefresh.setOnRefreshListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        BusManager.BUS.register(mParkingAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusManager.BUS.unregister(mParkingAdapter);
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void onParkingFound(DistantQueryFinishedEvent event) {
        mSwipeRefresh.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        ParkingManager.updateAvailabilityAndScheduleFromDistantApi();
    }
}
