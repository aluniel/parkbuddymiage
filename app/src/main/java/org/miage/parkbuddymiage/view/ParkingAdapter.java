package org.miage.parkbuddymiage.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import org.miage.parkbuddymiage.R;
import org.miage.parkbuddymiage.controller.ParkingManager;
import org.miage.parkbuddymiage.event.BusManager;
import org.miage.parkbuddymiage.event.ShowButtonsEvent;
import org.miage.parkbuddymiage.event.LocationChangedEvent;
import org.miage.parkbuddymiage.controller.LocationProviderService;
import org.miage.parkbuddymiage.controller.SearchInStringEngine;
import org.miage.parkbuddymiage.event.ParkingUpdatedEvent;
import org.miage.parkbuddymiage.event.ShowScreenEvent;
import org.miage.parkbuddymiage.model.Parking;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

class ParkingAdapter extends ListAdapter<Parking, ParkingAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<Parking> mList;

    private RecyclerView mRecyclerView;
    private boolean areButtonsShown;

    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            showButtons(recyclerView.canScrollVertically(1));
        }
    };

    ParkingAdapter(Context context) {
        super(DIFF_UTIL);
        mContext = context;
        mInflater = LayoutInflater.from(context);
        ViewHolder.DISPONIBLE  = context.getString(R.string.disponible);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        mRecyclerView = recyclerView;
        mRecyclerView.addOnScrollListener(scrollListener);
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        mRecyclerView.removeOnScrollListener(scrollListener);
        mRecyclerView = null;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Parking parking = getItem(position);
        holder.update(parking);
    }

    private void showButtons(boolean show) {
        if(areButtonsShown == show)
            return;
        areButtonsShown = show;
        if(!show && mRecyclerView.computeVerticalScrollRange() <= mRecyclerView.getHeight())
            return;
        BusManager.BUS.post(new ShowButtonsEvent(show));
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void onLocationChanged(LocationChangedEvent event) {
        if(mList != null && !mList.isEmpty()) {
            Collections.sort(mList, new Comparator<Parking>() {
                @Override
                public int compare(Parking o1, Parking o2) {
                    if(o1.getCurrentDistanceFloat() == -1 && o2.getCurrentDistanceFloat() == -1)
                        return o1.name.get().compareTo(o2.name.get());
                    if(o1.getCurrentDistanceFloat() == -1)
                        return 1;
                    if(o2.getCurrentDistanceFloat() == -1)
                        return -1;
                    return Float.compare(o1.getCurrentDistanceFloat(), o2.getCurrentDistanceFloat());
                }
            });
        }
        submitList(mList);
        notifyDataSetChanged();
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void onSearchParkingEvent(final ParkingUpdatedEvent event) {
        ((Activity)mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mList = event.getParkings();
                submitList(mList);
                if(event.isForceDisplay())
                    notifyDataSetChanged();
            }
        });
    }

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        static int FAV_TRUE_COLOR  = Color.argb(255, 255, 200, 0);
        static int FAV_FALSE_COLOR = Color.argb(255, 255, 255, 255);
        static int RED_COLOR       = Color.argb(255, 150, 60, 60);
        static int GREEN_COLOR     = Color.argb(255, 30, 150, 50);
        static String DISPONIBLE;

        @BindView(R.id.elem_list_container)
        ConstraintLayout mLayout;
        @BindView(R.id.elem_list_name)
        TextView mName;
        @BindView(R.id.elem_list_address)
        TextView mAddress;
        @BindView(R.id.elem_list_city)
        TextView mCity;
        @BindView(R.id.elem_list_fav)
        ImageButton mFavorite;
        @BindView(R.id.elem_list_distance)
        TextView mDistance;
        @BindView(R.id.elem_list_places)
        TextView mPlacesLeft;
        @BindView(R.id.elem_list_credit_card)
        ImageView mCreditCard;
        @BindView(R.id.elem_list_cash)
        ImageView mCash;
        @BindView(R.id.elem_list_totalGR)
        ImageView mTotalGR;
        Parking mParking;

        ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            ButterKnife.bind(this, view);
        }

        void update(Parking parking) {
            mParking = parking;

            // Favoris
            mFavorite.setColorFilter(mParking.favorite.get() ? FAV_TRUE_COLOR : FAV_FALSE_COLOR);

            // Nom et adresse
            mName   .setText(SearchInStringEngine.getHighlightedText(mParking.name.get()       , ParkingManager.getFilter()));
            mAddress.setText(SearchInStringEngine.getHighlightedText(mParking.address.get()    , ParkingManager.getFilter()));
            mCity   .setText(SearchInStringEngine.getHighlightedText(mParking.getPostCodeCity(), ParkingManager.getFilter()));

            // Distance
            mDistance.setVisibility(LocationProviderService.INSTANCE().isUpdated() ? View.VISIBLE : View.GONE);
            mDistance.setText(mParking.getCurrentDistance());

            // Disponibilité
            mPlacesLeft.setText(String.format(DISPONIBLE, mParking.availableSlots.get()));
            mPlacesLeft.setTextColor(mParking.availableSlots.get() <= mParking.slotsDisplayFull.get() ? RED_COLOR : GREEN_COLOR);

            // Moyens de paiement
            mCreditCard    .setVisibility(mParking.ccPayment.get()   ? View.VISIBLE : View.GONE);
            mCash          .setVisibility(mParking.cashPayment.get() ? View.VISIBLE : View.GONE);
            mTotalGR       .setVisibility(mParking.grPayment.get()   ? View.VISIBLE : View.GONE);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if(position != RecyclerView.NO_POSITION) {
                BusManager.BUS.post(ShowScreenEvent.Parking(mParking));
            }
        }

        @OnClick(R.id.elem_list_fav)
        public void onFavClick() {
            mParking.favorite.set(!mParking.favorite.get());
            ParkingManager.setFavorite(mParking.id.get(), mParking.favorite.get());
            mFavorite.setColorFilter(mParking.favorite.get() ? FAV_TRUE_COLOR : FAV_FALSE_COLOR);
        }
    }

    private static final DiffUtil.ItemCallback<Parking> DIFF_UTIL = new DiffUtil.ItemCallback<Parking>() {

        @Override
        public boolean areItemsTheSame(Parking oldParking, Parking newParking) {
            return oldParking.id.get() == newParking.id.get();
        }

        @Override
        @SuppressWarnings({"RedundantIfStatement"})
        public boolean areContentsTheSame(Parking oldParking, Parking newParking) {
            // Nom
            if(!Objects.equals(oldParking.name.get(), newParking.name.get()))
                return false;
            // Adresse
            if(!Objects.equals(oldParking.address.get(), newParking.address.get()))
                return false;
            if(!Objects.equals(oldParking.postCode.get(), newParking.postCode.get()))
                return false;
            if(!Objects.equals(oldParking.city.get(), newParking.city.get()))
                return false;
            // Favoris
            if(oldParking.favorite.get() != newParking.favorite.get())
                return false;
            // Disponibilité
            if(oldParking.availableSlots.get() != newParking.availableSlots.get())
                return false;
            if(oldParking.totalSlots.get() != newParking.totalSlots.get())
                return false;
            // Paiement
            if(oldParking.cashPayment.get() != newParking.cashPayment.get())
                return false;
            if(oldParking.ccPayment.get() != newParking.ccPayment.get())
                return false;
            if(oldParking.grPayment.get() != newParking.grPayment.get())
                return false;
            // Distance
            if(LocationProviderService.INSTANCE().isUpdated() && oldParking.getLastCurrentDistance() != newParking.getCurrentDistanceFloat())
                return false;

            return true;
        }
    };
}
