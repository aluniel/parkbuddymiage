package org.miage.parkbuddymiage.view;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.squareup.otto.Subscribe;

import org.miage.parkbuddymiage.R;
import org.miage.parkbuddymiage.controller.ParkingManager;
import org.miage.parkbuddymiage.event.ShowButtonsEvent;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import lombok.Getter;
import lombok.experimental.Accessors;

@Accessors(prefix = {"m"})
public class DataViewFragment extends BaseFragment {

    public static DataViewFragment newInstance() {
        DataViewFragment fragment = new DataViewFragment();
        return fragment;
    }

    @Getter
    private DataScreen mScreen;

    private Fragment mSelectedFragment;
    private Fragment mListFragment;
    private Fragment mMapFragment;

    @Getter
    @BindView(R.id.fav_selector)
    ImageButton mFavSelector;

    @Getter
    @BindView(R.id.search_cancel_layout)
    RelativeLayout mSearchCancel;

    @BindView(R.id.fav_selector_layout)
    RelativeLayout mFavSelectorLayout;

    @Getter
    @BindView(R.id.activity_list_input_search)
    EditText mInputSearch;

    private Handler mHandler;
    private Instant lastRun;

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            final int interval = 30 * 1000;
            try {
                if(lastRun == null || lastRun.isBefore(Instant.now().minus(interval, ChronoUnit.MILLIS))) {
                    ParkingManager.updateAvailabilityAndScheduleFromDistantApi();
                }
            } finally {
                mHandler.postDelayed(mStatusChecker, interval);
            }
        }
    };

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        setScreen(mScreen, true);
    }

    @Override
    public void onResume() {
        super.onResume();
        mHandler = new Handler();
        startRepeatingTask();
        //ParkingManager.getParkingsFromLocalDB();
        mSearchCancel.setVisibility(ParkingManager.isSearchActivated() ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void onPause() {
        super.onPause();
        stopRepeatingTask();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        setScreen(mScreen, true);
        return view;
    }

    @OnClick(R.id.fav_selector)
    @SuppressWarnings("unused")
    public void onFavSelectorClick() {
        ImageButton favSelector = mFavSelector;
        if(favSelector.getTag() != null && (Boolean)favSelector.getTag()) {
            favSelector.setColorFilter(ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.favUnselected));
            favSelector.setTag(false);
        } else {
            favSelector.setColorFilter(ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.favSelected));
            favSelector.setTag(true);
        }
        ParkingManager.setFavOnly(getFavSelectorValue());
    }
    
    @OnClick(R.id.search_cancel)
    @SuppressWarnings("unused")
    public void onSearchCancelClick() {
        ParkingManager.disableSearch();
        mSearchCancel.setVisibility(View.INVISIBLE);
    }

    @OnTextChanged(value = R.id.activity_list_input_search, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void afterInputSearchTextChanged(Editable editable) {
        ParkingManager.setFilter(editable.toString());
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void onAskButtonVisibilityChanged(ShowButtonsEvent event) {
        mFavSelectorLayout.setVisibility(event.show ? View.VISIBLE : View.INVISIBLE);
        mSearchCancel.setVisibility(event.show && ParkingManager.isSearchActivated() ? View.VISIBLE : View.INVISIBLE);
    }

    public boolean getFavSelectorValue() {
        if(mFavSelector == null)
            return false;
        if(mFavSelector.getTag() == null)
            return false;
        return (Boolean)mFavSelector.getTag();
    }

    public void setScreen(DataScreen screen) {
        setScreen(screen, false);
    }

    private void setScreen(DataScreen screen, boolean force) {
        if(!force && mScreen == screen)
            return;
        mScreen = screen;
        if(!isAdded())
            return;
        boolean addToBackStack = true;
        switch (screen) {
            case List:
                if(mListFragment == null)
                    mListFragment = ListFragment.newInstance();
                if(mSelectedFragment == mListFragment)
                    return;
                mSelectedFragment = mListFragment;
                break;
            case Map:
                if(mMapFragment == null)
                    mMapFragment = MapFragment.newInstance();
                if(mSelectedFragment == mMapFragment)
                    return;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mFavSelectorLayout.setVisibility(View.VISIBLE);
                        mSearchCancel.setVisibility(ParkingManager.isSearchActivated() ? View.VISIBLE : View.INVISIBLE);
                    }
                });
                mSelectedFragment = mMapFragment;
                break;
            default:
                return;
        }
        if(addToBackStack)
            getChildFragmentManager().beginTransaction().replace(R.id.data_container, mSelectedFragment).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).addToBackStack(null).commit();
        else
            getChildFragmentManager().beginTransaction().replace(R.id.data_container, mSelectedFragment).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
    }

    public enum DataScreen {
        List,
        Map
    }
}
