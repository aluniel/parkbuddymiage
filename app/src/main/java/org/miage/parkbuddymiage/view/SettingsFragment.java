package org.miage.parkbuddymiage.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.view.View;

import org.miage.parkbuddymiage.R;
import org.miage.parkbuddymiage.model.Settings;

import butterknife.BindView;
import butterknife.OnCheckedChanged;

/**
 * Created by alexa on 20/11/2018.
 */

public class SettingsFragment extends BaseFragment {

    @BindView(R.id.fragment_settings_switch_notif)
    SwitchCompat mSwitchNotif;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            mSwitchNotif.setChecked(Settings.INSTANCE.isNotificationActivated());
        } catch(Exception ignored) { }
    }

    @OnCheckedChanged(R.id.fragment_settings_switch_notif)
    public void onSwitchNotif() {
        try {
            Settings.INSTANCE.changeNotificationActivated(mSwitchNotif.isChecked());
        } catch (Exception ignored) { }
    }
}
