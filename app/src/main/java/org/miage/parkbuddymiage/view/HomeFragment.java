package org.miage.parkbuddymiage.view;

import android.os.Bundle;

import org.miage.parkbuddymiage.R;
import org.miage.parkbuddymiage.event.BusManager;
import org.miage.parkbuddymiage.event.ShowScreenEvent;

import butterknife.OnClick;

public class HomeFragment extends BaseFragment {

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @OnClick(R.id.button_list)
    protected void list_click() {
        BusManager.BUS.post(ShowScreenEvent.Screen(ShowScreenEvent.Screen.List));
    }

    @OnClick(R.id.button_maps)
    protected void maps_click() {
        BusManager.BUS.post(ShowScreenEvent.Screen(ShowScreenEvent.Screen.Map));
    }

    @OnClick(R.id.button_search)
    protected void search_click() {
        BusManager.BUS.post(ShowScreenEvent.Screen(ShowScreenEvent.Screen.Search));
    }

    @OnClick(R.id.button_settings)
    protected void settings_click() {
        BusManager.BUS.post(ShowScreenEvent.Screen(ShowScreenEvent.Screen.Settings));
    }

}
