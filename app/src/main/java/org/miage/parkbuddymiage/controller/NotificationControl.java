package org.miage.parkbuddymiage.controller;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import org.miage.parkbuddymiage.R;
import org.miage.parkbuddymiage.model.Parking;
import org.miage.parkbuddymiage.view.BaseActivity;

import static android.provider.Settings.System.getString;
import static android.support.v4.content.ContextCompat.getSystemService;

public class NotificationControl {

    private static Context mContext;

    public static void createNotificationChannel(Context context) {
        mContext = context;
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Notif parking";
            String description = "Parking à proximité";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel("25", name, importance);
            channel.setDescription(description);
            channel.setImportance(importance);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(context, NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    static void notifyUser(Parking parking) {
        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(mContext, BaseActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("toDataParkingFragment", "true");
        intent.putExtra("idParking", parking.id.get());
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, 0);


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext, "25")
                .setSmallIcon(R.drawable.ic_application)
                .setContentTitle("Un parking n'est pas loin ! (500m) ")
                .setContentText("Il reste " + parking.availableSlots.get() + " places dans " + parking.name.get())
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND|Notification.DEFAULT_LIGHTS|Notification.DEFAULT_VIBRATE)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(mContext);
        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(25, mBuilder.build());
    }
}
