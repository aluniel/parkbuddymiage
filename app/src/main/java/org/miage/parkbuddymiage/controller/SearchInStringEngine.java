package org.miage.parkbuddymiage.controller;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;

public class SearchInStringEngine {

    static int HIGHLIGHT_COLOR = Color.argb(255, 61, 70, 147);

    public static boolean searchInString(String string, String filter) {
        if(string == null || filter == null)
            return false;
        if(filter.isEmpty())
            return false;
        String lString = string.toLowerCase();
        String lFilter = filter.toLowerCase();
        int posString = 0;
        int posFilter = 0;
        int maxString = lString.length() - 1;
        int maxFilter = lFilter.length() - 1;
        while(posString <= maxString) {
            if(lFilter.charAt(posFilter) == lString.charAt(posString)) {
                posFilter++;
                if(posFilter > maxFilter)
                    return true;
            }
            posString++;
        }
        return false;
    }

    public static SpannableString getHighlightedText(String string, String filter) {
        filter = filter.toLowerCase();
        SpannableString spannableString = new SpannableString(string);

        BackgroundColorSpan[] backgroundSpans = spannableString.getSpans(0, spannableString.length(), BackgroundColorSpan.class);
        for(BackgroundColorSpan span: backgroundSpans) {
            spannableString.removeSpan(span);
        }

        if(filter.isEmpty() || !searchInString(string, filter))
            return spannableString;

        String lString = string.toLowerCase();
        String lFilter = filter.toLowerCase();
        int posString = 0;
        int posFilter = 0;
        int maxString = lString.length() - 1;
        int maxFilter = lFilter.length() - 1;

        while(posString <= maxString) {
            if(lFilter.charAt(posFilter) == lString.charAt(posString)) {
                spannableString.setSpan(new BackgroundColorSpan(HIGHLIGHT_COLOR), posString, posString + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                posFilter++;
                if(posFilter > maxFilter)
                    break;
            }
            posString++;
        }

        return spannableString;
    }
}
