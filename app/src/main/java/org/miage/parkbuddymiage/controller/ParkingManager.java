package org.miage.parkbuddymiage.controller;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.miage.parkbuddymiage.event.BackgroundTaskUpdatedEvent;
import org.miage.parkbuddymiage.event.BusManager;
import org.miage.parkbuddymiage.event.DisplayToastEvent;
import org.miage.parkbuddymiage.event.DistantQueryFinishedEvent;
import org.miage.parkbuddymiage.event.ParkingUpdatedEvent;
import org.miage.parkbuddymiage.model.ModelConverter;
import org.miage.parkbuddymiage.model.Parking;
import org.miage.parkbuddymiage.model.ParkingDataBase;
import org.miage.parkbuddymiage.model.opendatanantes.AvailabilitySearchResult;
import org.miage.parkbuddymiage.model.opendatanantes.ParkingSearchRESTService;
import org.miage.parkbuddymiage.model.opendatanantes.ParkingSearchResult;
import org.miage.parkbuddymiage.model.opendatanantes.ScheduleSearchResult;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@ToString
@Accessors(prefix = "m")
public abstract class ParkingManager {

    /////////////////////////
    // Retrofit            //
    /////////////////////////

    /**
     * Service retrofit
     */
    private final static ParkingSearchRESTService mParkingSearchRESTService;

    static {
        Gson gsonConverter = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(new OkHttpClient())
                .baseUrl(ParkingSearchRESTService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                .build();

        mParkingSearchRESTService = retrofit.create(ParkingSearchRESTService.class);
    }

    /////////////////////////
    // Recherche           //
    /////////////////////////

    /**
     * Filtre de recherche rapide
     */
    @Getter
    private static String mFilter = "";
    /**
     * Indique si seul les favoris sont affichés
     */
    @Getter
    private static boolean mFavOnly;

    /**
     * Filtre de recherche sur le nom (isEmpty() si inactif)
     */
    @Getter @Setter(AccessLevel.PRIVATE)
    private static String mNameFilter = "";
    /**
     * Filtre de recherche sur l'adresse (isEmpty() si inactif)
     */
    @Getter @Setter(AccessLevel.PRIVATE)
    private static String mAddressFilter = "";
    /**
     * Filtre de recherche sur la capacité (0 si inactif)
     */
    @Getter @Setter(AccessLevel.PRIVATE)
    private static int mCapacityFilter = 0;
    /**
     * Filtre de recherche sur le paiement par carte bancaire (false si inactif)
     */
    @Getter @Setter(AccessLevel.PRIVATE)
    private static boolean mCCPaymentFilter = false;
    /**
     * Filtre de recherche sur le paiement en espèce (false si inactif)
     */
    @Getter @Setter(AccessLevel.PRIVATE)
    private static boolean mCashPaymentFilter = false;
    /**
     * Filtre de recherche sur le paiement par carte Total GR (false si inactif)
     */
    @Getter @Setter(AccessLevel.PRIVATE)
    private static boolean mGRPaymentFilter = false;

    public static boolean isSearchActivated() {
        return !mNameFilter.isEmpty() || !mAddressFilter.isEmpty() || mCapacityFilter > 0 || mCCPaymentFilter || mCashPaymentFilter || mGRPaymentFilter;
    }

    public static void disableSearch() {
        mNameFilter        = "";
        mAddressFilter     = "";
        mCapacityFilter    = 0;
        mCCPaymentFilter   = false;
        mCashPaymentFilter = false;
        mGRPaymentFilter   = false;
        getParkingsFromLocalDB();
    }

    public static void activateSearch(String name, String address, int capacity, boolean ccPayment, boolean cashPayment, boolean grPayment) {
        mNameFilter        = name;
        mAddressFilter     = address;
        mCapacityFilter    = capacity;
        mCCPaymentFilter   = ccPayment;
        mCashPaymentFilter = cashPayment;
        mGRPaymentFilter   = grPayment;
        getParkingsFromLocalDB();
    }

    public static void setFilter(String filter) {
        mFilter = filter;
        getParkingsFromLocalDB(true);
    }

    public static void setFavOnly(boolean favOnly) {
        mFavOnly = favOnly;
        getParkingsFromLocalDB();
    }

    /////////////////////////
    // Distant queries     //
    /////////////////////////

    public static void updateParkingsFromDistantApi() {
        getParkingsFromLocalDB();
        BusManager.BUS.post(new BackgroundTaskUpdatedEvent(true));
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    Response<ParkingSearchResult> response = mParkingSearchRESTService.searchForParkings(0).execute();
                    List<Parking> parkings = new ArrayList<>(ModelConverter.convertOpenDataNantesModelToParkBuddyMiageModel(response.body()));
                    while(response.body().getNhits() > response.body().getNextStart()) {
                        response = mParkingSearchRESTService.searchForParkings(response.body().getNextStart()).execute();
                        parkings.addAll(ModelConverter.convertOpenDataNantesModelToParkBuddyMiageModel(response.body()));
                    }
                    for(Parking parking : parkings) {
                        parking.favorite.set(ParkingDataBase.get().isParkingFavorite(parking.id.get()));
                    }
                    ParkingDataBase.get().update(parkings);
                    updateAvailabilityAndScheduleFromDistantApi(false);
                } catch (Exception ex) {
                    BusManager.BUS.post(new DisplayToastEvent("Impossible de mettre à jour les parkings"));
                    Log.e("[ParkingSearcher][REST]", "Unable to update parkings : " + ex.getMessage());
                    BusManager.BUS.post(new BackgroundTaskUpdatedEvent(false));
                    BusManager.BUS.post(new DistantQueryFinishedEvent());
                }
                return null;
            }
        }.execute();
    }

    public static void updateAvailabilityAndScheduleFromDistantApi() {
        updateAvailabilityAndScheduleFromDistantApi(true);
    }

    private static void updateAvailabilityAndScheduleFromDistantApi(final boolean triggerLocalDB) {
        if(triggerLocalDB)
            getParkingsFromLocalDB();
        BusManager.BUS.post(new BackgroundTaskUpdatedEvent(true));
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                boolean infoLoaded = false;
                try {
                    Response<AvailabilitySearchResult> response = mParkingSearchRESTService.searchForAvailability(0).execute();
                    List<AvailabilitySearchResult.AvailabilityResult> tmpResults = response.body().getRecords();
                    while(response.body().getNhits() > response.body().getNextStart()) {
                        response = mParkingSearchRESTService.searchForAvailability(response.body().getNextStart()).execute();
                        tmpResults.addAll(response.body().getRecords());
                    }
                    List<Parking> parkings = ParkingDataBase.get().getAll();
                    ModelConverter.addAvailabilityToParkBuddyMiageModel(parkings, tmpResults);
                    ParkingDataBase.get().update(parkings);
                    infoLoaded = true;
                } catch (Exception ex) {
                    BusManager.BUS.post(new DisplayToastEvent("Impossible de mettre à jour les disponibilités"));
                    Log.e("[ParkingSearcher][REST]", "Unable to update availability : " + ex.getMessage());
                }

                try {
                    Response<ScheduleSearchResult> response = mParkingSearchRESTService.searchForSchedule(0).execute();
                    List<ScheduleSearchResult.ScheduleResult> tmpResults = response.body().getRecords();
                    while(response.body().getNhits() > response.body().getNextStart()) {
                        response = mParkingSearchRESTService.searchForSchedule(response.body().getNextStart()).execute();
                        tmpResults.addAll(response.body().getRecords());
                    }
                    List<Parking> parkings = ParkingDataBase.get().getAll();
                    ModelConverter.addScheduleToParkBuddyMiageModel(parkings, tmpResults);
                    ParkingDataBase.get().update(parkings);
                    infoLoaded = true;
                } catch (Exception ex) {
                    BusManager.BUS.post(new DisplayToastEvent("Impossible de mettre à jour les horaires"));
                    Log.e("[ParkingSearcher][REST]", "Unable to update schedules : " + ex.getMessage());
                }
                if(infoLoaded)
                    getParkingsFromLocalDB();
                BusManager.BUS.post(new BackgroundTaskUpdatedEvent(false));
                BusManager.BUS.post(new DistantQueryFinishedEvent());
                return null;
            }
        }.execute();
    }

    /////////////////////////
    // Local queries       //
    /////////////////////////

    public static void getParkingsFromLocalDB() {
        getParkingsFromLocalDB(false);
    }

    private static void getParkingsFromLocalDB(final boolean forceDisplay) {
        new AsyncTask<AsyncTaskSearchParams, Void, Void>() {
            @Override
            protected Void doInBackground(AsyncTaskSearchParams... params) {
                AsyncTaskSearchParams param = params[0];
                List<Parking> parkings;
                if(param.isSearchActivated()) {
                    parkings = ParkingDataBase.get().getAll(param.isFavOnly(), param.getFilter(),
                            param.getNameFilter(), param.getAddressFilter(), param.getCapacityFilter(),
                            param.isCCPaymentFilter(), param.isCashPaymentFilter(), param.isGRPaymentFilter());
                }
                else {
                    parkings = ParkingDataBase.get().getAll(param.isFavOnly(), param.getFilter());
                }
                BusManager.BUS.post(new ParkingUpdatedEvent(parkings, forceDisplay));
                return null;
            }
        }.execute(createSearchParams());
    }

    public static AsyncTask<Void, Void, Parking> getParkingsFromLocalDB(final int id) {
        AsyncTask<Void, Void, Parking> task = new AsyncTask<Void, Void, Parking>() {
            @Override
            protected Parking doInBackground(Void... params) {
                return ParkingDataBase.get().getParking(id);
            }
        };
        task.execute();
        return task;
    }

    public static void setFavorite(final int id, final boolean favorite) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                ParkingDataBase.get().updateFavorite(id, favorite);
                return null;
            }
        }.execute();
    }

    @AllArgsConstructor
    private static class AsyncTaskSearchParams {
        /**
         * Filtre de recherche rapide
         */
        @Getter @Setter(AccessLevel.PRIVATE)
        private String mFilter;
        /**
         * Indique si seul les favoris sont affichés
         */
        @Getter @Setter(AccessLevel.PRIVATE)
        private boolean mFavOnly;

        /**
         * Indique si la recherche est active
         */
        @Getter @Setter(AccessLevel.PRIVATE)
        private boolean mSearchActivated;

        /**
         * Filtre de recherche sur le nom (isEmpty() si inactif)
         */
        @Getter @Setter(AccessLevel.PRIVATE)
        private String mNameFilter;
        /**
         * Filtre de recherche sur l'adresse (isEmpty() si inactif)
         */
        @Getter @Setter(AccessLevel.PRIVATE)
        private String mAddressFilter;
        /**
         * Filtre de recherche sur la capacité (0 si inactif)
         */
        @Getter @Setter(AccessLevel.PRIVATE)
        private int mCapacityFilter;
        /**
         * Filtre de recherche sur le paiement par carte bancaire (false si inactif)
         */
        @Getter @Setter(AccessLevel.PRIVATE)
        private boolean mCCPaymentFilter;
        /**
         * Filtre de recherche sur le paiement en espèce (false si inactif)
         */
        @Getter @Setter(AccessLevel.PRIVATE)
        private boolean mCashPaymentFilter;
        /**
         * Filtre de recherche sur le paiement par carte Total GR (false si inactif)
         */
        @Getter @Setter(AccessLevel.PRIVATE)
        private boolean mGRPaymentFilter;
    }

    private static AsyncTaskSearchParams createSearchParams() {
        return new AsyncTaskSearchParams(mFilter, mFavOnly, ParkingManager.isSearchActivated(),
                mNameFilter, mAddressFilter, mCapacityFilter,
                mCCPaymentFilter, mCashPaymentFilter, mGRPaymentFilter);
    }
}
