package org.miage.parkbuddymiage.controller;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.miage.parkbuddymiage.event.BusManager;
import org.miage.parkbuddymiage.event.LocationChangedEvent;
import org.miage.parkbuddymiage.event.NotifyUserEvent;
import org.miage.parkbuddymiage.model.Parking;
import org.miage.parkbuddymiage.model.ParkingDataBase;
import org.miage.parkbuddymiage.model.Settings;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import lombok.Getter;
import lombok.experimental.Accessors;
import pub.devrel.easypermissions.EasyPermissions;

public class LocationProviderService {
    /**
     * Instance du singleton
     */
    @Getter
    @Accessors(prefix = "", fluent = true)
    private final static LocationProviderService INSTANCE = new LocationProviderService();

    /**
     * Service de localisation
     */
    private FusedLocationProviderClient mLocationService;

    /**
     * true si les coordonnées ont été mises à jour au moins une fois
     */
    @Getter
    private boolean updated;

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            if(locationResult == null)
                return;
            Location location = locationResult.getLastLocation();
            mLatitude  = location.getLatitude();
            mLongitude = location.getLongitude();

            updated = true;

            BusManager.BUS.post(new LocationChangedEvent());
            if(Settings.INSTANCE != null && Settings.INSTANCE.isNotificationActivated())
                checkDistance();
        }
    };

    private void checkDistance() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                for(Parking parking : ParkingDataBase.get().getAll()) {
                    if(parking.getCurrentDistanceFloat() != -1 && parking.getCurrentDistanceFloat() <= 500) {
                        if(!contains(parking.id.get())) {
                            alreadyNotified.add(parking.id.get());
                            BusManager.BUS.post(new NotifyUserEvent(parking));
                        }
                    } else {
                        remove(parking.id.get());
                    }
                }
                return null;
            }

            private boolean contains(int id) {
                for(Integer integer : alreadyNotified) {
                    if(integer.intValue() == id) {
                        return true;
                    }
                }
                return false;
            }

            private void remove(int id) {
                for(Integer integer : alreadyNotified) {
                    if(integer.intValue() == id) {
                        alreadyNotified.remove(integer);
                        return;
                    }
                }
            }
        }.execute();
    }

    /**
     * Latitude actuelle
     */
    private double mLatitude;

    /**
     * Liste des parkings précédemment notifié
     */
    private List<Integer> alreadyNotified = new ArrayList<>();

    /**
     * Longitude actuelle
     */
    private double mLongitude;

    /**
     *
     */
    private boolean isProviding;

    public static void init(Context context) {
        INSTANCE().mLocationService = LocationServices.getFusedLocationProviderClient(context);
    }

    @SuppressLint("MissingPermission")
    public void startProviding() {
        if(isProviding)
            return;
        if(!EasyPermissions.hasPermissions(mLocationService.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION))
            return;
        LocationRequest request = LocationRequest.create();
        request.setInterval(1000);
        request.setFastestInterval(1000);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationService.requestLocationUpdates(request, mLocationCallback, null);
        isProviding = true;
    }

    public void endProviding() {
        if(isProviding)
            mLocationService.removeLocationUpdates(mLocationCallback);
    }

    public String getCurrentDistanceString(float distance) {
        if(distance == -1)
            return "";
        return new DecimalFormat("#.00").format(distance / 1000) + " km";
    }

    public float getCurrentDistance(double latitude, double longitude) {
        if(!updated)
            return -1;
        float[] results = new float[1];
        Location.distanceBetween(mLatitude, mLongitude, latitude, longitude, results);
        return results[0];
    }
}
