package org.miage.parkbuddymiage.event;

import org.miage.parkbuddymiage.model.Parking;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class NotifyUserEvent {
    public final Parking parking;
}
