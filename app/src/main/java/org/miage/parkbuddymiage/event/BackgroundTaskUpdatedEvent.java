package org.miage.parkbuddymiage.event;

public class BackgroundTaskUpdatedEvent {
    public final boolean taskBeginning;

    public BackgroundTaskUpdatedEvent(boolean taskBeginning) {
        this.taskBeginning = taskBeginning;
    }
}
