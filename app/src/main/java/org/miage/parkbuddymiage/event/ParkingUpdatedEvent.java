package org.miage.parkbuddymiage.event;

import org.miage.parkbuddymiage.model.Parking;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import lombok.Getter;
import lombok.experimental.Accessors;

@Accessors(prefix = {"m"})
public class ParkingUpdatedEvent {

    @Getter
    private List<Parking> mParkings;

    @Getter
    private boolean mForceDisplay;

    public ParkingUpdatedEvent(List<Parking> parkings, boolean forceDisplay){
        build(parkings, forceDisplay);
    }

    private void build(List<Parking> parkings, boolean forceDisplay) {
        mForceDisplay  = forceDisplay;
        this.mParkings = parkings;
        Collections.sort(mParkings, new Comparator<Parking>() {
            @Override
            public int compare(Parking o1, Parking o2) {
                if(o1.getCurrentDistanceFloat() == -1 && o2.getCurrentDistanceFloat() == -1)
                    return Objects.requireNonNull(o1.name.get()).compareTo(Objects.requireNonNull(o2.name.get()));
                if(o1.getCurrentDistanceFloat() == -1)
                    return 1;
                if(o2.getCurrentDistanceFloat() == -1)
                    return -1;
                return Float.compare(o1.getCurrentDistanceFloat(), o2.getCurrentDistanceFloat());
            }
        });
    }
}
