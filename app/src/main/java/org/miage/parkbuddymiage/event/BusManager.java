package org.miage.parkbuddymiage.event;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by alexa on 20/11/2018.
 */

public class BusManager {
    public static Bus BUS = new Bus(ThreadEnforcer.ANY);
}
