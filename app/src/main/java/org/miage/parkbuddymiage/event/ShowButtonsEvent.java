package org.miage.parkbuddymiage.event;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ShowButtonsEvent {
    public boolean show;
}
