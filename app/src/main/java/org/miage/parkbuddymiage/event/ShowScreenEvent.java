package org.miage.parkbuddymiage.event;

import org.miage.parkbuddymiage.model.Parking;

import lombok.Getter;
import lombok.experimental.Accessors;

@Accessors(prefix = {"m"})
public class ShowScreenEvent {

    public static ShowScreenEvent Screen(Screen screen) {
        return new ShowScreenEvent(screen);
    }

    public static ShowScreenEvent Parking(Parking parking) {
        return new ShowScreenEvent(parking.id.get());
    }

    public static ShowScreenEvent Parking(int id) {
        return new ShowScreenEvent(id);
    }

    @Getter
    private Screen mScreen;

    @Getter
    private int mParking;

    private ShowScreenEvent(Screen screen) {
        mScreen = screen;
    }

    private ShowScreenEvent(int id) {
        mScreen  = Screen.Parking;
        mParking = id;
    }

    public enum Screen {
        Home,
        List,
        Map,
        Parking,
        Search,
        Settings
    }
}
