package org.miage.parkbuddymiage.event;

import lombok.Getter;
import lombok.experimental.Accessors;

@Accessors(prefix = {"m"})
public class DisplayToastEvent {
    @Getter
    private String mMessage;

    public DisplayToastEvent(String message) {
        mMessage = message;
    }
}
